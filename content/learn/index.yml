---
  title: "GitLab Learn"
  description: "Learn new skills while working with GitLab. Our easy-to-use learning platform provides instructions and feedback throughout your journey."
  certifications_link:
    title: "Learn more about GitLab Certifications"
    link: "https://about.gitlab.com/learn/certifications/"
    data_ga_name: "learn more"
    data_ga_location: "header"
  free_trial_link:
    title: "Get free trial"
    link: "https://gitlab.com/-/trials/new?_gl=1%2Aiun8xs%2A_ga%2AMjYyMTI5MzcuMTYyNjk3NTQwMg..%2A_ga_ENFH3X7M5Y%2AMTY2MzY5MTAzNi43Mi4wLjE2NjM2OTEwMzYuMC4wLjA.&glm_content=default-saas-trial&glm_source=about.gitlab.com"
    data_ga_name: "free trial"
    data_ga_location: "header"
  reset_all: "Reset all"
  use_case: "Use-case"
  level: "Level"
  pace: "Self-paced"
  assessment: "Assessment"
  registration: "Registration"
  confidentiality: "Confidentiality"
  maintainer: "Maintainer"
  programs:
    - name: "TeamOps Certification"
      url: https://levelup.gitlab.com/learn/course/teamops
      description: |
        TeamOps is a new people practice that brings precision and operations to how people work together.
      live_date: 2022-10-01
      maintainer: "Learning & Development"
      level: "Intermediate"
      use_case: "Talent & Development"
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Free"
      confidentiality: "Public"

    - name: "Get started with GitLab tutorials"
      url: https://docs.gitlab.com/ee/tutorials/
      description: |
        Get started with GitLab tutorials is a handpicked set of tutorials and resources to help you learn how to use GitLab. It includes topics on Finding your way around GitLab, Use Git, Plan your work in projects, Use CI/CD pipelines, and Secure your application.
      live_date: "2022-01-13"
      maintainer: "Technical Writing"
      level: "Beginner"
      use_case:
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab CI/CD - Go Tanuki workshop"
      url: https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/#template-basics-and-cicd-with-the-go-tanuki
      description: |
        Learn GitLab Basics & the first steps to a Go application fixed and built with CI/CD. You'll also practice security. Exercises and solutions are provided in the project and slides.
      live_date:
      maintainer: "Developer Evangelism"
      level: "Beginner"
      use_case: "Continuous Integration"
      pace: "Self-Paced"
      assessment: "None"
      registration: "None"
      confidentiality: "Public"

    - name: "Efficient DevSecOps Pipelines in a Cloud Native World workshop"
      url: https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/#efficient-devsecops-pipelines-in-a-cloud-native-world
      description: |
        Learn about CI/CD pipeline effciency and how to analyse and monitor problems. You'll dive into practical examples in order to optimize your CI/CD configuratio, optimize resource usage, and make your CI/CD infrastructure more efficient, including auto-scaling ideas. The full day workshop includes many best practices, with all exercises and solutions provided in the slides and project.
      live_date:
      maintainer: "Developer Evangelism"
      level: "Intermediate"
      use_case: "Continuous Integration"
      pace: "Self-Paced"
      assessment: "None"
      registration: "None"
      confidentiality: "Public"

    - name: "Practical Kubernetes Monitoring with Prometheus workshop"
      url: https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/#practical-kubernetes-monitoring-with-prometheus
      description: |
        Learn how to monitor Kubernetes with Prometheus using the Prometheus Operator and kube-prometheus. Practice to deploy and instrument your own Python application, and learn more about service discovery, alerts, SLOs, observability and security in a cloud native environment. The slides contain exercises and solutions for async practice.
      live_date:
      maintainer: "Developer Evangelism"
      level: "Intermediate"
      use_case: "Observability"
      pace: "Self-Paced"
      assessment: "None"
      registration: "None"
      confidentiality: "Public"

    - name: "DevOps with GitLab CI Course - Build Pipelines and Deploy to AWS"
      url: https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/#devops-with-gitlab-ci-course---build-pipelines-and-deploy-to-aws
      description: |
        This FreeCodeCamp course will teach you how to use GitLab CI to create CI/CD pipelines for building and deploying software to AWS. Original author is Valentin Despa, GitLab hero, who collaborated and reviewed together with Michael Friedrich on the Developer Evcangelism team.
      live_date:
      maintainer: "Developer Evangelism"
      level: "Beginner"
      use_case: "Continuous Integration"
      pace: "Self-Paced"
      assessment: "None"
      registration: "None"
      confidentiality: "Public"

    - name: "Technical Writing Fundamentals"
      url: https://levelup.gitlab.com/learn/course/gitlab-technical-writing-fundamentals
      description: |
        The GitLab Technical Writing Fundamentals course is available to help both GitLab and community contributors write and edit documentation for the product. This course provides direction on grammar, writing, and topic design.
      live_date: "2021-08-02"
      maintainer: "Engineering"
      level: "Beginner"
      use_case: "Talent Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "Backwards Compatibility Training"
      url: https://levelup.gitlab.com/learn/course/backwards-compatibility-training
      description: With multi-node setups, at any given time two versions of the application can run, the code needs to be backward compatible in order to support zero-downtime upgrades. Learn about the common causes of compatibility problems during upgrades and best practices to prevent them.
      live_date:
      maintainer: "Engineering"
      level: "Beginner"
      use_case: "Version Control & Collaboration"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab 101"
      url: https://levelup.gitlab.com/learn/course/gitlab101
      description: |
        This training is geared toward GitLab team members and the wider community who are in non-engineering roles (i.e. recruiting, peopleops, marketing, finance, etc) and/or have not used a DevOps tool like GitLab before. This can also be helpful for non-engineering people outside of GitLab wanting to learn how to use GitLab for personal projects.
      live_date: "2020-02-01"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Agile Planning"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab 201"
      url: https://levelup.gitlab.com/learn/course/gitlab-201-certification
      description: |
        This training is a deep dive into GitLab Epics and Merge requests for team members and wider community members getting started using the GitLab tool
      live_date: "2020-08-01"
      maintainer: "Learning & Development"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "Remote Foundations Certfication"
      url: https://levelup.gitlab.com/learn/course/remote-foundations
      description: |
        Presented at a self-directed pace, the GitLab remote certification is designed to give new managers and individual contributors an opportunity to master all-remote business concepts and build key skills in remote subject areas.
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Talent Development"
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Free"
      confidentiality: "Public"

    - name: "Bias towards Async Communication"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/bias-towards-asynchronous-communication
      description: |
        This pathway is designed to reinforce our value of a bias towards asynchronous communication. Everyone at GitLab is responsible for being a role model of our values. This pathway will help learners understand our practices for asynchronous communication at GitLab even better!
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "GitLab Values"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Internal"

    - name: "GitLab Values"
      url: https://levelup.gitlab.com/learn/course/gitlab-values
      description: |
        Review of Gitlab values for team members and community members
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "GitLab Values"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "360 Feedback Process at GitLab"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/360-feedback-process-at-gitlab
      description: |
        Communicate the value and structure of 360 review cycle at GitLab
      live_date: "2021-08-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Talent Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Internal"

    - name: "Building an Individual Growth Plan"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/building-an-individual-growth-plan
      description: |
        How to build an IGP for team members
      live_date: "2021-08-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Talent Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Internal"

    - name: "Giving and Receiving Feedback"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/giving-feedback
      description: |
        Strategies for giving and receiving feedback at GitLab
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Leadership Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Internal"

    - name: "Coaching for Performance"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/coaching-for-performance
      description: |
        Handbook resources for how managers can manager performance of direct reports
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Leadership Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Internal"

    - name: "Running an effective 1:1"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/running-an-effective-11
      description: |
        Reviews strategies for how to run an effective 1:1 in an all remote company
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Leadership Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Internal"

    - name: "Building high performing teams"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/building-high-performing-teams-orientation
      description: |
        How to build a high performing all remote team training for people managers
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Leadership Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Internal"

    - name: "GitLab Certified Git Associate"
      url: https://about.gitlab.com/services/education/gitlab-certified-associate/
      description:
      live_date: "2020-04-01"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case: "Version Control & Collaboration"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified CI/CD Associate"
      url: https://about.gitlab.com/services/education/gitlab-cicd-associate/
      description:
      live_date: "2020-06-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Continuous Integration"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified CI/CD Specialist"
      url:
      description:
      live_date: "2022-09-10"
      maintainer: "PS Education Services"
      level: "Advanced"
      use_case: "Continuous Integration"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified DevOps Professional"
      url: https://about.gitlab.com/services/education/gitlab-certified-devops-pro/
      description:
      live_date: "2020-12-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified InnerSourcing Specialist"
      url: https://about.gitlab.com/services/education/gitlab-innersourcing-specialist/
      description:
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case: "Version Control & Collaboration"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified Project Management Associate"
      url: https://about.gitlab.com/services/education/gitlab-project-management-associate/
      description:
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified Security Specialist"
      url: https://about.gitlab.com/services/education/gitlab-security-specialist/
      description:
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab with Git Basics Training"
      url: https://about.gitlab.com/services/education/gitlab-basics/
      description:
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case: "Version Control & Collaboration"
      pace: "Both"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab CI/CD Training"
      url: https://about.gitlab.com/services/education/gitlab-ci/
      description:
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Continuous Delivery/Release"
      pace: "Both"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab for Project Managers Training"
      url: https://about.gitlab.com/services/education/pm/
      description:
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Security Essentials Training"
      url: https://about.gitlab.com/services/education/security-essentials/
      description:
      live_date: "2020-12-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Both"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab DevOps Fundamentals Training"
      url: https://about.gitlab.com/services/education/devops-fundamentals/
      description:
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Advanced"
      use_case: "DevSecOps"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab InnerSourcing Training"
      url: https://about.gitlab.com/services/education/innersourcing-course/
      description:
      live_date: "2020-03-01"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case: "Version Control & Collaboration"
      pace: "Both"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab System Administration Training"
      url: https://about.gitlab.com/services/education/admin/
      description:
      live_date: "2020-02-01"
      maintainer: "PS Education Services"
      level: "Advanced"
      use_case:
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Advanced CI/CD Training"
      url:
      description:
      live_date:
      maintainer: "PS Education Services"
      level: "Advanced"
      use_case: "Continuous Delivery/Release"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Implementation Services Specialist Certification"
      url: https://about.gitlab.com/services/pse-certifications/implementation-specialist/
      description:
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Simplify DevOps"
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Internal"

    - name: "GitLab Migration Services Specialist Certification"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/gitlab-certified-migration-specialist-bundle
      description:
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Simplify DevOps"
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Internal"

    - name: "GitLab Certified System Admin Associate"
      url:
      description:
      live_date: "2022-08-30"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case:
      pace: "Instructor-Led"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Team Members Certification"
      url: https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/gitlab-team-members-certification
      description: |
        This course is designed to provide team members with an introduction to GitLab.
      live_date: "2021-10-31"
      maintainer: "Learning and Development"
      level: "Beginner"
      use_case:
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Internal"
