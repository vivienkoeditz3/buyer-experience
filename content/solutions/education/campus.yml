---
  title: GitLab for Campuses - Collaboration made easy
  description: GitLab enables campuses to modernize, innovate, and secure quickly with an all-in-one platform where everyone can contribute.
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab Solutions for DevOps in Education
        subtitle:  Bring the digital transformation to your campus with the only complete DevOps platform
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: /free-trial/
          text: Start your free trial
          data_ga_name: free trial
          data_ga_location: header
        secondary_btn:
          url: /sales/
          text: Contact Sales
          data_ga_name: contact
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt: "students from above"
          rounded: true
    - name: copy-media
      data:
        block:
          - header: Collaborate. Innovate. Transform.
            no_max_height: true
            hide_horizontal_rule: true
            aos_animation: fade-up
            aos_duration: 500
            icon_list:
              - header: Deploy faster and more frequently.
                text: Automate pipelines with Continuous Integration. Increase consistency and repeatability with Continuous Delivery.
                icon:
                  name: arrow-circle
                  variant: marketing
                  alt: Arrow Circle Icon
                  hex_color: '#EF6523'
              - header: Secure in real time.
                text: Address security at every stage and spend less time fixing issues.
                icon:
                  name: lock-alt-3
                  variant: marketing
                  alt: Lock Icon
                  hex_color: '#FBA226'
            text: |
              GitLab enables campuses to modernize, innovate, and secure quickly with an all-in-one platform where everyone can contribute.
            image:
              image_url: /nuxt-images/enterprise/gitlab-enterprise-panel-collaboration.png
              alt: "Image: Enterprise Panel"
            image_mobile:
              image_url: /nuxt-images/enterprise/small-business-code.jpg
              alt: "Image: Enterprise Panel"
    - name: education-pricing
      data:
        header: How can you bring GitLab to your Campus?
        header_animation: zoom-in-up
        header_animation_duration: 800
        text: Check out our options
        aos_animation: zoom-in-up
        aos_duration: 800
        plan_free:
          title: GitLab for Education
          description: |
            Free Education licenses to support teaching, learning, and research*
          features:
            - Unlimited seats
            - Top tiers
            - Any deployment method
            - 50000 CI/CD minutes
            - A community of support
            - Classroom or research use only
          link:
            text: Apply now
            href: /solutions/education/join
            data_ga_name: Apply now
            data_ga_location: pricing
          footnote: |
            * It is not authorized for use to run, administer, or operate an institution.
        plan_silver_premium:
          title: GitLab for Campuses
          description: |
            The complete DevOps platform for the whole campus for one simplified price.
          features:
            - Unlimited seats**
            - Top tiers
            - Any deployment method
            - 50000 CI/CD minutes
            - Priority support
            - No use case restrictions
          link:
            text: Contact Sales Today
            href: /sales
            data_ga_name: pricing
            data_ga_location: contact sales today
          footnote: |
            ** Up to student enrollment
        plan_gold_ultimate:
          title: Academic Discount
          description: |
            20% discount off list price for all tiers
          features:
            - Just the seats you need
            - Any tier
            - Any deployment method
            - Priority support
            - CI/CD minutes according to tier purchased
            - No use case restrictions
          link:
            text: Contact Sales Today
            href: /sales
            data_ga_name: pricing academic discount
            data_ga_location: contact sales today
    - name: benefits
      data:
        aos_animation: fade-up
        aos_duration: 800
        full_background: true
        text_align: 'left'
        cards_per_row: 2
        subtitle: How does GitLab help Educational Institutions become more productive, efficient, and effective?
        header_icon:
          name: gitlab
          variant: marketing
          alt: "Image: Icon Tanuki"
        benefits:
          - icon:
              name: branch-circle
              alt: Branch Icon
              variant: marketing
              hex_color: '#64A7F0'
            title: Information Technology
            description: |
              Automate the process of software delivery and infrastructure changes with the complete DevOps platform

              * Continuous Integration and delivery enable your IT department to build high-quality applications at scale
              * Accelerate your institution’s digital transformation. Real-time security monitoring
              * Breakdown silos and streamline efficiency
            link:
              text: See how University of Washington does it
              url: /customers/uw/
          - icon:
              name: collaboration-link
              alt: Collaboration Link Icon
              variant: marketing
              hex_color: '#FCA326'
            title: Institution Administration
            description: |
              GitLab is a collaboration tool designed to help people work better across distributed teams.

              * From conception through implementation, every department can deliver faster using one unified application
              * Track work using GitLab Boards, Labels, Epics, and Roadmaps
              * Store internal processes with issues and repositories
            link:
              text: See how EAB does it
              url: /customers/EAB/
          - icon:
              name: search-circle
              alt: Search Icon
              variant: marketing
              hex_color: '#FA7035'
            title: Research, Libraries, and Learning Technology
            description: |
              GitLab brings transparency, shared ownership, operational efficiency, and a single source of truth to your research allowing you to focus on what matters - the science.

              * End-to-end visibility, centralized access, and repeatability
              * Improve code quality and documentation
              * Containerization, CI/CD and shared repositories make research and infrastructure reusable
            link:
              text: See how Square Kilometer Array does it
              url: /customers/square_kilometre_array/
          - icon:
              name: graduate
              alt: Graduate Icon
              variant: marketing
              hex_color: '#27AE60'
            title: Empower Students
            description: |
              An invaluable tool for providing students in-depth and practical knowledge on how the DevOps lifecycle works in today’s professional world.

              * Evolve from coding on paper or local drives to a software platform that provides code management, continuous integration, and transparency
              * Teach the best the industry has to offer with GitLab CI/CD and SCM
              *  Prepare the future workforce for collaboration, efficiency, and transparency
            link:
              text: See how Dublin City University does it
              url: /customers/dublin-city-university/

