---
  title: GitLab for the Public Sector
  description: Social coding, continuous integration, and release automation have proven to accelerate development and software quality to meet mission objectives. Learn more!
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for Public Sector
        subtitle: The one DevOps platform to accelerate your speed to mission
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Questions? Contact us
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/public-sector.jpg
          image_url_mobile: /nuxt-images/solutions/public-sector.jpg
          alt: "Image: gitlab for public sector"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: University of Washington Logo
            image: /nuxt-images/logos/uw-logo.svg
            url: https://about.gitlab.com/customers/uw/
            aria_label: Link to University of Washington customer case study
          - name: US Army Logo
            image: /nuxt-images/logos/usarmy.svg
            url: https://about.gitlab.com/customers/us_army_cyber_school/
            aria_label: Link to US Army Cyber School customer case study
          - name: Cook County Logo
            image: /nuxt-images/logos/cookcounty-logo.svg
            url: https://about.gitlab.com/customers/cook-county/
            aria_label: Link to Cook County customer case study
          - name: University of Surrey Logo
            image: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            url: https://about.gitlab.com/customers/university-of-surrey/
            aria_label: Link to University of Surrey customer case study
          - name: EAB Logo
            image: /nuxt-images/logos/eab-logo.svg
            url: https://about.gitlab.com/customers/EAB/
            aria_label: Link to E.A.B. customer case study
          - name: Victoria University Wellington Logo
            image: /nuxt-images/logos/victoria-university-wellington-logo.svg
            url: https://about.gitlab.com/customers/victoria_university/
            aria_label: Link to Victoria University Wellington customer case study
    - name: 'side-navigation'
      links:
        - title: Overview
          href: '#overview'
        - title: Testimonials
          href: '#testimonials'
        - title: Capabilities
          href: '#capabilities'
        - title: Benefits
          href: '#benefits'
        - title: Case Studies
          href: '#case-studies'
      slot_enabled: true
      slot_offset: 2
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content: 
            - name: 'by-solution-benefits'
              data:
                title: Security. Efficiency. Control.
                image:
                  image_url: "/nuxt-images/solutions/benefits/by-solution-benefits-public-sector.jpeg"
                  alt: Collaboration image
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: Devsecops Icon
                      variant: marketing
                    header: Reduce security and compliance risk
                    text: Discover security and compliance flaws early in the process while enforcing consistent guardrails throughout the entire DevOps lifecycle.
                    link_text: Learn more about DevSecOps
                    link_url: /solutions/dev-sec-ops
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: repo-code
                      alt: Repo Code Icon
                      variant: marketing
                    header: Free up resources
                    text: Better serve the civilian experience and contain global threats by eliminating fragile and complex DIY toolchains that impede collaboration and innovation.
                    link_text: Why GitLab
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: digital-transformation
                      alt: Digital Transformation Icon
                      variant: marketing
                    header: Modernize while you simplify
                    text: Use one DevOps platform designed to meet the unique needs of cloud-native applications and the infrastructure upon which they rely.
                    link_text: Learn more about our platform approach
                    link_url: /solutions/devops-platform/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              offset: 1
              data:
                header: |
                  Trusted by government.
                  <br />
                  Loved by developers.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/usarmy.svg
                      alt: US Army
                    quote: Instead of having to teach people 10 different things, they just learn one thing, which makes it much easier in the long run.
                    author: Chris Apsey
                    position: Captain, U.S. Army
                    ga_carousel: public-sector us army
                  - title_img:
                      url: /nuxt-images/logos/uw-logo.svg
                      alt: University of Washington Logo
                    quote: Over the past two years, GitLab has been transformational for our organization here at UW. Your platform is fantastic!
                    author: Aaron Timss
                    position: Director of Information, CSE
                    ga_carousel: public-sector university of washington
                  - title_img:
                      url: /nuxt-images/logos/cookcounty-logo.svg
                      alt: Cook County Logo
                    quote: With GitLab, I can produce research and show people in the office. They suggest changes to the research and I can make those changes without having to worry about version control and saving my work. One repository makes it so that I can focus more on the actual work and less on the mechanics of working.
                    author: Robert Ross
                    position: Chief Data Officer
                    ga_carousel: public-sector cook county
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              offset: 2
              data:
                subtitle: A complete DevOps platform for Public Sector
                sub_description: 'Starting with one DevOps platform that includes secure and robust source code management (SCM), continuous integration (CI), continuous delivery (CD), and continuous software security and compliance, GitLab addresses your unique needs such as these:'
                white_bg: true
                sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
                alt: benefits image
                solutions:
                  - title: SBOM
                    description: Review your project’s software bill of materials with key details about the dependencies used, including their known vulnerabilities.
                    icon:
                      name: less-risk
                      alt: Less Risk Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: Zero Trust
                    description: Learn how GitLab is following Zero Trust principals and demonstrating best practices.
                    icon:
                      name: monitor-pipeline
                      alt: Monitor Pipeline Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: /handbook/security/
                    data_ga_name: zero trust
                    data_ga_location: solutions block
                  - title: Vulnerability management
                    description: Manage your software vulnerabilities all in one place — within the pipeline, for the project, groups of projects, and across your groups.
                    icon:
                      name: shield-check
                      alt: Shield Check Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: Fuzz testing
                    description: GitLab allows you to add fuzz testing to your pipelines, alongside a comprehensive set of scanners. Fuzz testing sends random inputs to an instrumented version of your application in order to cause unexpected behavior. This behavior indicates security and logic flaws that should be addressed.
                    icon:
                      name: monitor-test
                      alt: Monitor Test Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: Off-line environments
                    description: Even when disconnected from the internet, you can run most of the GitLab security scanners.
                    icon:
                      name: gitlab-monitor-alt
                      alt: Monitor GitLab Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/offline_deployments/
                    data_ga_name: offline environment
                    data_ga_location: solutions block
                  - title: Common controls for compliance
                    description: Automate and enforce common policies like separation of duties, protected branches, and push rules.
                    icon:
                      name: shield-check
                      alt: Shield Check Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
                  - title: Compliance pipelines
                    description: Enforce pipeline scan configurations to ensure required security scans are not circumvented.
                    icon:
                      name: shield-check
                      alt: Shield Check Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: compliance
                    data_ga_location: solutions block
                  - title: Low to High development
                    description: Enable collaboration among varied development teams.
                    icon:
                      name: cog-code
                      alt: Cog Code Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: https://www.youtube.com/watch?v=HSfDTslLRT8/
                    data_ga_name: low to high
                    data_ga_location: solutions block
                  - title: On-prem, self-hosted, or SaaS
                    description: GitLab works in all environments. The choice is yours.
                    icon:
                      name: gitlab-monitor-alt
                      alt: Monitor Gitlab Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: /pricing/
                    data_ga_name: on-prem self-hosted or saas
                    data_ga_location: solutions block
                  - title: Hardened container image
                    description: DoD-compliant hardened container image minimizes the risk profile, enables more secure applications to be deployed quickly, and supports continuous authority to operate processes; also accepted into the Iron Bank.
                    icon:
                      name: lock-cog
                      alt: Lock Cog Icon
                      variant: marketing
                    link_text: Learn More
                    link_url: /press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html
                    data_ga_name: hardened container image
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Uniquely suited to the Public Sector
                cards:
                  - title: NIST SSDF
                    description: GitLab is aligned to NIST’s guidance, helping CIOs implement the required actions for software supply chain security to proactively defend their agencies. Learn more about how <a href="/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/" data-ga-name="gitlab meets nist" data-ga-location="value prop">GitLab meets NIST SSDF 1.1 guidance</a>.
                    icon:
                      name: less-risk
                      alt: Less Risk Icon
                      variant: marketing
                  - title: The DI2E alternative
                    description: Access to DI2E, Defense Intelligence Information Enterprise, has been canceled, forcing agencies to rethink their entire DevSecOps model. GitLab is a solid alternative to DI2E and our single application simplifies procurement.
                    icon:
                      name: devsecops
                      alt: DevSecOps Icon
                      variant: marketing
                  - title: Supply chain visibility and control
                    description: GitLab’s One DevOps Platform is delivered as a single, <a href="/press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html" data-ga-name="hardened" data-ga-location="value prop">hardened</a> application that simplifies end-to-end visibility and traceability. Security and compliance policies are managed and enforced consistently across all of your DevOps processes.
                    icon:
                      name: eye-magnifying-glass
                      alt: Eye Magnifying Glass Icon
                      variant: marketing
                  - title: On-prem, self-hosted, or SaaS
                    description: The choice is yours.
                    icon:
                      name: monitor-web-app
                      alt: Monitor Web App Icon
                      variant: marketing
        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: Customer Realized Benefits
                rows:
                  - title: U.S. Army Cyber School
                    subtitle: How the U.S. Army Cyber School created “Courseware as Code” with GitLab
                    image:
                      url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
                      alt: Soldier speaking on radio
                    button:
                      href: /customers/us_army_cyber_school/
                      text: Learn more
                      data_ga_name: us army learn more
                      data_ga_location: case studies
                  - title: Cook County Assessor’s Office
                    subtitle: How Chicago’s Cook County assesses economic data with transparency and version control
                    image:
                      url: /nuxt-images/blogimages/cookcounty.jpg
                      alt: Houses as seen from above
                    button:
                      href: /customers/cook-county/
                      text: Learn more
                      data_ga_name: cook country learn more
                      data_ga_location: case studies
                  - title: University of Washington
                    subtitle: The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects.
                    image:
                      url: /nuxt-images/blogimages/uw-case-study-image.png
                      alt: University of Washington Campus
                    button:
                      href: /customers/uw/
                      text: Learn more
                      data_ga_name: uw learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'GitLab Events'
        description: "Join an event to learn how your team can deliver software faster and more efficiently, while strengthening security and compliance. We'll be attending, hosting, and running numerous events in 2022, and can’t wait to see you there!"
        link: /events/
        image: /nuxt-images/events/pubsec-event-link.jpeg
        alt: crowd during a presentation
        icon: calendar-alt-2
