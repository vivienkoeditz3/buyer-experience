window.onload = (_event) => {
  const cookieBannerExists = document.getElementById('onetrust-banner-sdk');
  if (cookieBannerExists && cookieBannerExists.style.display !== 'none') {
    // eslint-disable-next-line no-undef, func-names
    drift.on('ready', function (_api, _payload) {
      // eslint-disable-next-line no-undef
      drift.config({
        verticalOffset: 130,
      });
    });
  }
};
