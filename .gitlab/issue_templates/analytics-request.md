## Context
*Example:* I’m redesigning the footer section and would like to understand how users are interacting with the existing elements.

**Related Issue:** \[Link to a related issue\]

## Analytics Questions
Provide a list of questions where data can help inform decision-making. 

*Example:*
* How many users viewed the footer section?
* How many users clicked on a footer link? 
* Can the data be broken down by device type? 

## Report Date Range 
*Example:* Q2 compared to Q1 since our quarterly OKR target is to increase social media link clicks from the footer. 

## Segment (optional)
*Example:* Let’s exclude logged-in users to narrow down prospects. 

## Priority
*Example:* I need to submit my design by 12/25, and I would like data to inform my decision-making a week prior. Please complete the report by 12/18 at the latest. 
